


(function () {
    'use strict';



    module.exports = function ( grunt ) {


        require('load-grunt-tasks')(grunt);



        var gruntInitConfig = {};



        // beg: jshint
        gruntInitConfig.jshint = {};

        gruntInitConfig.jshint.options = {};

        gruntInitConfig.jshint.options.jshintrc = './.jshintrc';

        gruntInitConfig.jshint.all = [
            'Gruntfile.js',
            'detect-element-resize.js',
            'jquery.resize.js'
        ];
        // end: jshint



        // beg: uglify
        gruntInitConfig.uglify = {
            'dev': {
                files: [
                    {
                        expand: true,
                        src: [
                            'detect-element-resize.js',
                            'jquery.resize.js'
                        ],
                        extDot: 'last',
                        ext: '.min.js'
                    }
                ]
            }
        };
        // end: uglify



        gruntInitConfig.watch = {
            dev: {
                files: [ 'detect-element-resize.js', 'jquery.resize.js' ],
                tasks: [ 'jshint' ]
            }
        };



        grunt.initConfig( gruntInitConfig );



        grunt.registerTask ( 'pro', [
            'jshint',
            'uglify'
        ] );



        grunt.registerTask( 'default', [
            'jshint',
            'watch'
        ] );
    };
}());